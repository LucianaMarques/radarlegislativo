# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import (
    Comissao,
    ComissaoCamara,
    ComissaoSenado,
    Deputado,
    Partido,
    Senador,
)


class ComissaoAdmin(admin.ModelAdmin):
    search_fields = ('nome', )


admin.site.register(Comissao, ComissaoAdmin)
admin.site.register(ComissaoCamara)
admin.site.register(ComissaoSenado)
admin.site.register(Deputado)
admin.site.register(Partido)
admin.site.register(Senador)
