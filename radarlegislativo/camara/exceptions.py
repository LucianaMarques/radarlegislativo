class ClientConnectionError(Exception):
    '''
        Exception to be raised when camara API response contains an error.
    '''
