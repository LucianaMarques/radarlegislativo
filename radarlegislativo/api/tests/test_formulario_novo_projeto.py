# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import TestCase, override_settings

from api.forms import FormularioProjeto
from main.models import PalavraChave, Tag


PALAVRAS_CHAVE = {
    "gdpr": "Privacidade",
    "censura": u"Liberdade de expressão",
    "marco civil": "Privacidade"
}


class TestCampoDeFormularioTags(TestCase):
    fixtures = ["tags"]

    @override_settings(RASPADOR_API_TOKEN="foobar")
    @override_settings(PALAVRAS_CHAVE=PALAVRAS_CHAVE)
    def test_campo_tag_com_palavras_chaves_existentes(self):
        data = {
            "id_site": "42",
            "origem": "SE",
            "palavras_chave": "gdpr, marco civil, censura",
            "token": "foobar"
        }
        nomes_tags = ("Privacidade", u"Liberdade de expressão")
        form = FormularioProjeto(data)

        self.assertTrue(form.is_valid())
        keywords, tags = form.cleaned_data['palavras_chave']

        self.assertEqual(2, len(tags))
        self.assertEqual(3, len(keywords))

        for t in Tag.objects.filter(nome__in=nomes_tags):
            self.assertIn(t, tags)

        for k in PalavraChave.objects.all():
            self.assertIn(k, keywords)

    @override_settings(PALAVRAS_CHAVE=PALAVRAS_CHAVE)
    @override_settings(RASPADOR_API_TOKEN="foobar")
    def test_sem_palavras_chave(self):
        data = {
            "id_site": "42",
            "origem": "SE",
            "token": "foobar"
        }
        form = FormularioProjeto(data)
        self.assertFalse(form.is_valid())
