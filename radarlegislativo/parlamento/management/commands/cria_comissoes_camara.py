# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
from django.core.management.base import BaseCommand
from tapioca_camara import Camara

from parlamento.models import ComissaoCamara


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = u"Popula banco com todas as comissões do Camara"

    def add_arguments(self, parser):
        parser.add_argument("--tipos", dest="tipos", default=[1, 2, 3, 4])

    def handle(self, *args, **options):
        camara = Camara()
        logger.info('Importando comissões do Câmara')
        paginacao = {'itens': 100, 'pagina': 1, 'idTipoOrgao': options['tipos']}
        resposta = camara.orgaos().get(params=paginacao)
        proxima_pagina = True
        while proxima_pagina:
            for comissao in resposta.dados:
                nome = comissao.nome._data
                if len(nome) > 255:
                    nome = nome[:255]
                ComissaoCamara.objects.create(
                    id_api=comissao.id._data,
                    nome=nome,
                    sigla=comissao.sigla._data,
                    tipo=comissao.tipoOrgao._data,
                )
            paginacao['pagina'] += 1
            resposta = camara.orgaos().get(params=paginacao)
            proxima_pagina = any(l for l in resposta.links if l.rel._data == 'next')
