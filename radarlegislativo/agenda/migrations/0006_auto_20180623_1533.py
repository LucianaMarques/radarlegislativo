# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-06-23 15:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0005_auto_20180502_1841'),
        ('parlamento', '0002_comissao_comissaocamara_comissaosenado'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evento',
            name='comissao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='parlamento.Comissao'),
        ),
        migrations.DeleteModel(
            name='Comissao',
        ),
    ]
