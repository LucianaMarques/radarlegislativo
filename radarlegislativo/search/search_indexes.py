# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from haystack import indexes
from celery_haystack.indexes import CelerySearchIndex
from main.models import Projeto


class ProjetoIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    apresentacao = indexes.DateTimeField(model_attr='apresentacao')
    urgente = indexes.BooleanField(model_attr='urgente')
    promulgado = indexes.BooleanField(model_attr='promulgado')
    arquivado = indexes.BooleanField(model_attr='arquivado')
    origem = indexes.FacetCharField(model_attr='origem')
    categoria = indexes.FacetMultiValueField()
    impacto = indexes.FacetCharField(model_attr='impacto')
    comissao = indexes.CharField(model_attr='local')
    ultima_atualizacao = indexes.DateTimeField(model_attr='ultima_atualizacao')

    def get_model(self):
        return Projeto

    def index_queryset(self, using=None):
        return Projeto.publicados.all()

    def prepare_categoria(self, obj):
        return [tag.pk for tag in obj.tags.all()]
