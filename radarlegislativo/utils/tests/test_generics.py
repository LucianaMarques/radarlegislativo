# -*- coding: utf-8 -*-
import codecs

from unittest import TestCase

from utils.generics import decode


class DecodeTextTestCase(TestCase):

    def test_decode_utf8_text_correctly(self):
        encoded = codecs.encode(u'Pão com lingüiça é bom', 'utf-8')
        expected = codecs.decode(encoded, 'utf-8')

        text = decode(encoded)
        assert expected == text

    def test_decode_latin1_text_correctly(self):
        encoded = codecs.encode(u'Pão com lingüiça é bom', 'latin-1')
        expected = codecs.decode(encoded, 'latin-1')

        text = decode(encoded)
        assert expected == text

    def test_decode_returns_text_if_text_is_unicode(self):
        expected = u'Pão com lingüiça é bom'

        text = decode(expected)
        assert expected == text
